﻿#pragma strict

var Player : GameObject;
var Puissance = 1000.0;
var Orientation : int;

function Start () {
	Player = GameObject.Find("CubePlayer");
}

function Update () {
	
}

function OnCollisionStay(hit : Collision)
{
	if(hit.gameObject.tag  ==  "Player")
	{
		var Vect = null;
		switch(Orientation)
		{
			// X Droite
			case 1: Vect = Vector3(Puissance,0,0);
				break;
			//X gauche
			case 2: Vect = Vector3(-Puissance,0,0);
				break;
			// Z Haut
			case 3: Vect = Vector3(0,0,Puissance);
				break;
			//Z Bas
			case 4: Vect = Vector3(0,0,-Puissance);
				break;
		}
	    Player.rigidbody.AddForce(Vect);
	}
}