﻿#pragma strict

var LifeUsed = 0;
var currentLife = 0;
var RecapTemp : Array = null;
var Pseudo : String;
var numberLvl : int = 5;
var numberCurrentLvl : int;
var scoreString : String = null;
var newRecord : boolean = false;
var saveString :String = null;
var Player : GameObject;
var SpeedDifficult : float;
var skinFacteur : boolean;
var instruction : boolean;
var reloadLvl : int;
var Music : boolean;

function Awake() {
    // Do not destroy this game object:
	DontDestroyOnLoad(this);
}

function OneMoreLife()
{
	currentLife++;
	LifeUsed++;
	PlayerPrefs.SetInt("TotalLife",LifeUsed);
}

function NewLevel(temp : float, numeroLvl : int)
{
	// Penality of death : 3s by death
	var penality : float = 3.0;
	penality = currentLife * penality;
	numberCurrentLvl = numeroLvl;
	RecapTemp[numeroLvl-1]=temp + penality;
	WriteScore();
	//Save current lvl for reload it
	if(numeroLvl == numberLvl)
	{
		PlayerPrefs.SetInt("CurrentLvl",0);	
		PlayerPrefs.SetInt("TotalLife",0);
	}
	else{
		PlayerPrefs.SetInt("CurrentLvl",numeroLvl+1);
	}
}

function setMusic(val : int)
{
	if(val==1)
	{
		Music = true;
	}
	else
	{
		Music = false;
	}
}


function getMusic() : boolean
{
	return Music;
}

function setSkinfacteur(val : boolean)
{
	skinFacteur = val;
}

function getSkinfacteur() : boolean
{
	return skinFacteur;
}

function getReloadLvl() : int
{
	if(PlayerPrefs.HasKey("CurrentLvl"))
	{
		reloadLvl = PlayerPrefs.GetInt("CurrentLvl");	
	}
	else
	{
		reloadLvl = 0;
	}
	return reloadLvl;
}

function reloadLife()
{
	if(PlayerPrefs.HasKey("TotalLife"))
	{
		LifeUsed = PlayerPrefs.GetInt("TotalLife");	
	}
	else
	{
		LifeUsed = 0;
	}
}

function resetCurrentLifeUsed()
{
	currentLife = 0;
}

function setInstruction(val : boolean)
{
	instruction = val;
}

function getInstruction() : boolean
{
	return instruction;
}

function getLifeUsed(): int
{
	return LifeUsed;
}

function setLifeUsed(nb : int)
{
	LifeUsed = nb;
}

function getLifeUsedLevel() : int
{
	return currentLife;
}

function getTemps(number : int): float
{	
	return RecapTemp[number];
}

function setPseudo(name : String)
{	
	Pseudo = name;
}

function setSpeed(speed : float)
{
	SpeedDifficult = speed;
}

function getSpeed(): float
{
	return SpeedDifficult;
}

function getPseudo(): String
{	
	return Pseudo;
}

function Initialisation()
{
	for(var i : int = 0; i < getNumberLvl(); i++)
	{
		RecapTemp.Add(0);
	}
}

function Performance(numero : int) : String
{
	if(!PlayerPrefs.HasKey("Score"))
	{
		return "indef";
	}
	else
	{
		var saveScore = PlayerPrefs.GetString("Score");
		var saveScoreSplit = saveScore.Split("/"[0]);
		if(numero>saveScoreSplit.Length)
		{
			return "Indef";
		}
		else if(numero<getNumberLvl())
			 {
				return saveScoreSplit[numero];
			 }
			 else
			 {
				return "Indef";
		 	 }
	}
}

function WriteScore()
{
	scoreString = null;
	for(var i : int = 0; i < getNumberLvl(); i++)
	{
		scoreString = scoreString + getTemps(i);
		scoreString = scoreString + "/";
	}
	
	if(!PlayerPrefs.HasKey("Score"))
	{
		PlayerPrefs.SetString("Score",scoreString);
	}
	Save();
}

function Save()
{
	var newSauvegarde : String = null;
	var saveScore = PlayerPrefs.GetString("Score");
	saveString = saveScore;
	var saveScoreSplit = saveScore.Split("/"[0]);
	
	var playerScoreSplit = scoreString.Split("/"[0]);
	
	// Return the best time value of this lvl
	// If it's a new best time newRecord is true
	var valeurRecord = Calcul(playerScoreSplit[numberCurrentLvl-1],saveScoreSplit[numberCurrentLvl-1]);

	// Save the new Record
	if(newRecord)
	{
		for(var j : int = 0; j < getNumberLvl(); j++)
		{
			if(j == numberCurrentLvl-1)
			{
				newSauvegarde = newSauvegarde + valeurRecord;
				newSauvegarde = newSauvegarde + "/";	
			}
			else{
				newSauvegarde = newSauvegarde + saveScoreSplit[j];
				newSauvegarde = newSauvegarde + "/";	
			}
		}

		Player = GameObject.Find("CubePlayer");
		Player.GetComponent(BallControl).setRecord(newRecord);
		PlayerPrefs.SetString("Score",newSauvegarde);	
			
		newRecord=false;
	}

	saveString = PlayerPrefs.GetString("Score");

}
function Calcul(playerScoreCalcul : String ,saveScoreCalcul : String) : String
{
	var playerScoreInt : float = float.Parse(playerScoreCalcul);
	var saveScoreInt : float = float.Parse(saveScoreCalcul);

	//New record !!
	if(saveScoreInt==0)
	{
		newRecord = true;
		return playerScoreCalcul;	
	}
	else if((playerScoreInt < saveScoreInt) && playerScoreInt!=0)
		{
			newRecord = true;
			return playerScoreCalcul;
		}
		else
		{
			return saveScoreCalcul;
		}
}


function getNumberLvl(): int
{
	return numberLvl;
}

function CreationPseudo()
{
	if(!PlayerPrefs.HasKey("Name"))
	{
		PlayerPrefs.SetString("Name",Pseudo);
	}
	else
	{
		var ListePseudo : String = PlayerPrefs.GetString("Name");
		if(!ListePseudo.Contains(Pseudo))
		{
			ListePseudo = ListePseudo + "/";
			ListePseudo = ListePseudo + Pseudo; 
		}
		PlayerPrefs.SetString("Name",ListePseudo);
	}
}

function LastPseudoPlayed() : String
{
	var ListePseudo : String = PlayerPrefs.GetString("Name");
	var ListePseudoSplit = ListePseudo.Split("/"[0]);
	return ListePseudoSplit[ListePseudoSplit.Length-1];
}

function Start (){
	RecapTemp = new Array ();
	Initialisation();
}

function Update () {

}