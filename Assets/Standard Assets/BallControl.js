﻿#pragma strict

// Move object using accelerometer
	var speed =5000;
	//var speedDifficult : float;
	var currentLvl : String;
	var speedacceleration = 10.0;
	var dimensionBox = 0.3f;
	var GUIAffichage =false;
	var LoadLvl : String;
	var myTimer : float = 0.0;
	var End : boolean = false;
	var Stat : GameObject;
	var Sauvegarde = false;
	var currentLvlInt : int;
	var record : boolean = false;
	var pause : boolean = false;
	var oldVelocityX : float;
	var oldVelocityY : float;
	var oldVelocityZ : float;
	var iconPause : Texture2D;
	var MenuSkin : GUISkin;
	var GoodSound: AudioClip;
	var BadSound: AudioClip;
	var soundPlay : boolean = false;
	var timer : float = 0.5;
	var PauseMode : boolean = false;
	
function Start () {
	// Disable screen dimming
	Screen.sleepTimeout = SleepTimeout.NeverSleep;
	myTimer = 0.0;
	Stat = GameObject.Find("Stats");
	//speedDifficult = Stat.GetComponent(StatsPlayer).getSpeed();
}

function Update () {
	//Wait end start instruction before move
	if(!Stat.GetComponent(StatsPlayer).getInstruction())
	{
		//Input for keyboard
		if (Input.GetKey (KeyCode.UpArrow)) rigidbody.AddForce (Vector3.forward);
		if (Input.GetKey (KeyCode.DownArrow)) rigidbody.AddForce (Vector3.back);
		if (Input.GetKey (KeyCode.LeftArrow)) rigidbody.AddForce (Vector3.left);
		if (Input.GetKey (KeyCode.RightArrow)) rigidbody.AddForce (Vector3.right);

		var dir : Vector3 = Vector3.zero;

			// we assume that device is held parallel to the ground
			// and Home button is in the right hand
			
			// remap device acceleration axis to game coordinates:
			//  1) XY plane of the device is mapped onto XZ plane
			//  2) rotated 90 degrees around Y axis
			dir.z = Input.acceleration.y;
			dir.x = Input.acceleration.x;
			
			// clamp acceleration vector to unit sphere
			if (dir.sqrMagnitude > 1)
				dir.Normalize();
			
			// Make it move 10 meters per second instead of 10 meters per frame...
			dir *= Time.deltaTime;
				
			// Move object
	//		transform.Translate (dir * speed);
		//this.transform.rotation.z += Input.acceleration.y/speedacceleration;
		//this.transform.rotation.x += Input.acceleration.x/speedacceleration;
		//this.transform.position.z += Input.acceleration.y/speed;
		//this.transform.position.x += Input.acceleration.x/speed;
		rigidbody.AddForce(dir*speed);
		
		//Name of level
		var nameScene : String = Application.loadedLevelName;
		currentLvl = nameScene;
		
		//Timer
		if(!GUIAffichage)
		{
			if(!PauseMode)
			{
	      		myTimer += Time.deltaTime;
	      	}
	    }
    }
}

function setRecord(etat : boolean)
{
	record = etat;
}

/* Fonction permettant de mettre en pause le mouvement de la balle
	@param etat : true mise en pause du mouvement / false reprise du mouvement avec les valeurs pre pause
	@param affichage : true gui speciale pause
*/
function etatPause(etat : boolean, affichage : boolean)
{
	if(affichage)
	{
		pause = etat;
	}
	if(etat)
	{
		oldVelocityZ = rigidbody.velocity.z;
		oldVelocityX = rigidbody.velocity.x;
		oldVelocityY = rigidbody.velocity.y;
		//Set velocity at 0 for sleep rigibody object
		rigidbody.velocity.z = 0;
		rigidbody.velocity.x = 0;
		rigidbody.velocity.y = 0;
		rigidbody.Sleep();
		PauseMode = true;
	}
	else
	{
		rigidbody.velocity.z = oldVelocityZ;
		rigidbody.velocity.x = oldVelocityX;
		rigidbody.velocity.y = oldVelocityY;
		PauseMode = false;
	}
}

function OnGUI()
{

	GUI.skin = MenuSkin;
	var MiddleScreenWidth = Screen.width/2;
	var MiddleScreenHeight = Screen.height/2;
	var dimX = dimensionBox*Screen.width;
	var dimY = dimensionBox*Screen.height;
	var AffichageResultat = new Rect(MiddleScreenWidth-dimX,MiddleScreenHeight-dimY,2*dimX,2*dimY);
	var dimensionLabel = AffichageResultat.height - GUI.skin.box.fontSize;
	if(pause)
	{
		GUI.Box(AffichageResultat ,iconPause);
	}
	
	if(Stat.GetComponent(StatsPlayer).getInstruction())
	{
		GUI.Box(AffichageResultat ,"Welcome");
		var firstLigne = new Rect(AffichageResultat.x+10,AffichageResultat.y+GUI.skin.box.fontSize,AffichageResultat.width,dimensionLabel/4);
		
	  	GUI.Label(firstLigne,"The goal of this game is to reach as fast as possible the green hole. ");
	  	GUI.Label(new Rect(firstLigne.x,firstLigne.y+dimensionLabel/4,AffichageResultat.width,dimensionLabel/4),"Be careful to not fall in the another hole, it will cost time penality.");
		GUI.Label(new Rect(firstLigne.x,firstLigne.y+2*dimensionLabel/4,AffichageResultat.width,dimensionLabel/4),"Incline your device to move the ball. Good luck & Have fun !");
		if(GUI.Button(new Rect(MiddleScreenWidth-dimX/2,AffichageResultat.y + AffichageResultat.height,dimX,60),"OK"))
		{
			Stat.GetComponent(StatsPlayer).setInstruction(false);
		}
	
	}

	
	if(GUIAffichage)
	{		
		var TimerAround = Mathf.Round (myTimer*100) / 100;
		if(!Sauvegarde)
	  	{
	  		Stat.GetComponent(StatsPlayer).NewLevel(TimerAround ,currentLvlInt);
	  		Sauvegarde = true;
	  	}
	  	
	
		
		GUI.Box(AffichageResultat ,"Level complete");
		var Premierlabel = new Rect(AffichageResultat.x+10,AffichageResultat.y+GUI.skin.box.fontSize,2*dimX,dimensionLabel/4);
		
	  	GUI.Label(Premierlabel,"Your time is : " + TimerAround.ToString());
	  	GUI.Label(new Rect(Premierlabel.x,Premierlabel.y+dimensionLabel/4,2*dimX,dimensionLabel/4),"Number of life used : " + Stat.GetComponent(StatsPlayer).getLifeUsed().ToString());
	  	GUI.Label(new Rect(Premierlabel.x,Premierlabel.y+2*dimensionLabel/4,2*dimX,dimensionLabel/4),"Number of life used in this level : " + Stat.GetComponent(StatsPlayer).getLifeUsedLevel().ToString());
	  	GUI.Label(new Rect(Premierlabel.x,Premierlabel.y+3*dimensionLabel/4,2*dimX,dimensionLabel/4),"Total time of your level : " + Stat.GetComponent(StatsPlayer).getTemps(currentLvlInt-1).ToString());
	  	
	  	//Affichage d'un nouveau record
	  	if(record)
	  	{
	  		GUI.Label(new Rect(AffichageResultat.x+10,AffichageResultat.y+50,2*dimX,100),"New Record !!");
	  		//record = false;
	  	}
	  	//End : false -> next level / true -> end of the game
	  	if(!End)
	  	{
	  		if(Stat.GetComponent(StatsPlayer).getMusic())
	  		{
		  		if(!soundPlay)
		  		{	
		  			AudioSource.PlayClipAtPoint(GoodSound,gameObject.transform.position);
		  			soundPlay = true;
		  		}
	  		}   
	  		etatPause(true,false);
		  	if(GUI.Button(new Rect(MiddleScreenWidth-dimX/2,AffichageResultat.y + AffichageResultat.height,dimX,60),"Next level"))
		  	{
		  		//reset currentLifeUsed
			  	Stat.GetComponent(StatsPlayer).resetCurrentLifeUsed();
		  		Application.LoadLevel(LoadLvl);
		  	}
	  	}
	  	else
	  	{
	  		etatPause(true,false);
		  	if(GUI.Button(new Rect(MiddleScreenWidth-dimX/2,AffichageResultat.y + AffichageResultat.height,dimX,60),"Finish"))
		  	{
		  		Stat.GetComponent(StatsPlayer).setSkinfacteur(false);
		  		Application.LoadLevel("Main");
		  	}
	  	}
	}
}

function OnCollisionEnter(collision : Collision)
{
	/*Good hole next level
	save time of execution, update user's life
	*/
	if(collision.gameObject.tag == "Finish")
	{
	  print("collision occured");
	  var res : String = currentLvl.Replace("Lvl","");
	  var lvlsuivant : int = int.Parse(res);
	  currentLvlInt = lvlsuivant;
	  lvlsuivant++;
	  var LvlSuivantString : String = lvlsuivant.ToString();
	  print(LvlSuivantString);
	  LoadLvl = "Lvl"+LvlSuivantString;
	  if(lvlsuivant>Stat.GetComponent(StatsPlayer).getNumberLvl())
	  {
	  	/*End of the game
	  	all the lvl accomplish
	  	*/
	  	End = true;
		
	  }
	  //Affichage GUI
	  GUIAffichage = true;
	  
	}
	/* Wrong hole restart level
	+1 life used
	*/
	if(collision.gameObject.tag == "Fail")
	{
	  etatPause(true,false);
	  if(Stat.GetComponent(StatsPlayer).getMusic())
	  {
		  if(!soundPlay)
		  {	
		  		AudioSource.PlayClipAtPoint(BadSound,gameObject.transform.position);
		  		soundPlay = true;
		  }   
	  }
	  yield WaitForSeconds(timer);
	  Stat.GetComponent(StatsPlayer).OneMoreLife();
	  Application.LoadLevel(currentLvl);
	}
			
	/* Wrong hole restart level
	+1 life used
	*/
	if(collision.gameObject.tag == "Wall")
	{
	  print("collision occured Wall");
	  var oldX = rigidbody.velocity.x;
	  var oldY = rigidbody.velocity.y;
	  var oldZ = rigidbody.velocity.z;
	 // rigidbody.AddForce( -collision.relativeVelocity, ForceMode.VelocityChange );

	}
}
