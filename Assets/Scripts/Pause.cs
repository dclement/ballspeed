﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {
	public Texture2D logoPause = null;
	public Texture2D logoParam = null;
	public float heightPercent = 0.3f;
	public float DimensionTest = 0.3f;
	public string TextGui = "Testing";
	public GameObject Player = null;
	public bool EtatPause = false;
	public GameObject Stat = null;

	void OnGUI() {
		var MiddleScreenWidth = Screen.width/2;
		var MiddleScreenHeight = Screen.height/2;
		var dimensionBox = 0.3f;
		var dimX = dimensionBox*Screen.width;
		var dimY = dimensionBox*Screen.height;
		var AffichageResultat = new Rect(MiddleScreenWidth-dimX,MiddleScreenHeight-dimY,2*dimX,2*dimY);

		Rect r = new Rect(Screen.width - (Screen.height * heightPercent),
		                  
		                  0,
		                  
		                  Screen.height * heightPercent,
		                  
		                  Screen.height * heightPercent);

	

	
		if (logoPause)
		{
			//Restart when touch screen
			for (var i = 0; i < Input.touchCount; ++i) {
				if (Input.GetTouch(i).phase == TouchPhase.Ended  && EtatPause == true && Input.GetTouch(i).position.x >AffichageResultat.x && Input.GetTouch(i).position.x < (AffichageResultat.x + AffichageResultat.width) && Input.GetTouch(i).position.y >AffichageResultat.y && Input.GetTouch(i).position.y < (AffichageResultat.y + AffichageResultat.height)) {
					EtatPause = false;
					Player.GetComponent<BallControl>().etatPause(EtatPause,true);
					
				}
			}
			if(GUI.Button(r, logoPause)) {
				//PAUSE
				Player = GameObject.Find("CubePlayer");
				if(!EtatPause)
				{
					EtatPause = true;
					Player.GetComponent<BallControl>().etatPause(EtatPause,true);
				}
				else{
					EtatPause = false;
					Player.GetComponent<BallControl>().etatPause(EtatPause,true);
				}
			}
		}
		else{
			Debug.LogError("Please assign a texture on the inspector");
			return;
		}

		Rect r2 = new Rect(Screen.width - 2*(Screen.height * heightPercent),
		                  
		                  0,
		                  
		                  Screen.height * heightPercent,
		                  
		                  Screen.height * heightPercent);
		if (logoParam)
		{
			if(GUI.Button(r2, logoParam)) {
				Stat = GameObject.Find("Stats");
				Stat.GetComponent<StatsPlayer>().setSkinfacteur(false);
				Application.LoadLevel("Main");
			}
		}
		else{
			Debug.LogError("Please assign a texture on the inspector");
			return;
		}

		
		Rect rectGui = new Rect(0,
		                  
		                  0,
		                  
		                        Screen.height * DimensionTest,
		                  
		                        Screen.height * DimensionTest);
		//TextGui = "Test";
		TextGui = Screen.width.ToString();
		TextGui = TextGui + " , ";
		TextGui = TextGui + Screen.height.ToString();
		GUI.Label(rectGui, TextGui);
		
		  
	}
	// Use this for initialization
	void Start () {
		Screen.SetResolution(Screen.width, Screen.height, true);
		Player = GameObject.Find("CubePlayer");
	}
	
	// Update is called once per frame
	void Update () {
		float oldX = Player.transform.position.x;
		float oldZ = Player.transform.position.z;
		float oldY = camera.transform.position.y;
		camera.transform.position = new Vector3(oldX,oldY,oldZ);
	}
}
