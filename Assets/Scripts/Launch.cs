﻿using UnityEngine;
using System.Collections;

public class Launch : MonoBehaviour {

	public float widthPercent = 0.2f;
	public float heightPercent = 0.5f;
	public float ecart = 0.5f;
	public float Bord = 20;
	public Texture2D logo = null;
	public string stringToEdit = "";
	public GameObject Stat = null;
	public GUISkin MenuSkin;
	public bool GUIParametre = false;
	public bool GUIPerformance = false;
	public bool GUIContinuer = false;
	public float dimensionBox = 0.3f;
	public int nombreTotalPage = 2;
	public int numeropage = 1;
	public int NbParamPage = 4;
	private float hScrollbarValue;
	private bool start = true;
	public int facteurAffichage = 0;
	public bool resetSkin = false;
	public bool SoundOption = true;

	void Start()
	{

	}

	void resetValGui()
	{
		GUI.skin.box.fontSize = 35;
		GUI.skin.button.fontSize = 30;
		GUI.skin.label.fontSize = 25;
		GUI.skin.textField.fontSize = 20;
	}

	void OnGUI() {

		float WidthDimensionButton = Screen.width*widthPercent;
		float HeightDimensionButton = Screen.height*heightPercent;
		float MiddleScreenWidth = Screen.width/2;
		float MiddleScreenHeight = Screen.height/2;
		float dimX = dimensionBox*Screen.width;
		float dimY = dimensionBox*Screen.height;
		Rect AffichageResultat = new Rect(MiddleScreenWidth-dimX,MiddleScreenHeight-dimY,2*dimX,2*dimY);
		CreationStat();
		Stat = GameObject.Find("Stats");
		GUI.skin = MenuSkin;
		if(!resetSkin)
		{
			resetValGui();
			resetSkin = true;
		}
		if(!Stat.GetComponent<StatsPlayer>().getSkinfacteur())
		{
			facteurAffichage = ((Screen.width*GUI.skin.box.fontSize)/960);
			GUI.skin.box.fontSize = facteurAffichage;
			facteurAffichage = ((Screen.width*GUI.skin.button.fontSize)/960);
			GUI.skin.button.fontSize = facteurAffichage;
			facteurAffichage = ((Screen.width*GUI.skin.label.fontSize)/960);
			GUI.skin.label.fontSize = facteurAffichage;
			facteurAffichage = ((Screen.width*GUI.skin.textField.fontSize)/960);
			GUI.skin.textField.fontSize = facteurAffichage;
			Stat.GetComponent<StatsPlayer>().setSkinfacteur(true);
		}
		//Menu parametre
		if(GUIParametre)
		{
			Rect BackRectangle = new Rect(AffichageResultat.x+(AffichageResultat.width/2)-WidthDimensionButton/2,AffichageResultat.y + AffichageResultat.height,WidthDimensionButton,HeightDimensionButton);
			if (GUI.Button(BackRectangle,"Back"))
			{
				GUIParametre = false;
				int val;
				if(SoundOption)
				{
					val = 1; 
				}
				else
				{
					val = 0;
				}
				PlayerPrefs.SetInt("Music",val);
			}
			//GUI.HorizontalScrollbar(AffichageResultat, hScrollbarValue,1.0f,0.0f,10.0f);
			//hScrollbarValue = GUI.HorizontalScrollbar(Rect(0,0,100,20), hScrollbarValue, 1, 0, 100, "Scroll");
		//	scrollPosition = GUI.BeginScrollView(AffichageResultat,scrollPosition,AffichageResultat);
		
			SoundOption = GUI.Toggle (new Rect (MiddleScreenWidth,MiddleScreenHeight,150,100), SoundOption, "Music");

			
			//hScrollbarValue = GUI.HorizontalScrollbar (new Rect (25, 100, 300, 500), hScrollbarValue, 1.0f, 0.0f, 100.0f);
			//print(hScrollbarValue);
			//Stat.GetComponent<StatsPlayer>().setSpeed(hScrollbarValue);
			if (GUI.Button(new Rect(BackRectangle.x,BackRectangle.y-HeightDimensionButton,WidthDimensionButton,HeightDimensionButton),"Reset"))
			{
				PlayerPrefs.DeleteAll();
			}
		}
		else
		{
			//Menu performance
			if(GUIPerformance)
			{
				//Calcul the number of page need 
				//NbParamPage correspond to the number of print time per page 
				nombreTotalPage = Stat.GetComponent<StatsPlayer>().getNumberLvl()/NbParamPage;
				//Need one more page
				if(Stat.GetComponent<StatsPlayer>().getNumberLvl()%NbParamPage!=0)
				{
					nombreTotalPage++;
				}
				Rect BackRectangle = new Rect(AffichageResultat.x+(AffichageResultat.width/2)-WidthDimensionButton/2,AffichageResultat.y + AffichageResultat.height,WidthDimensionButton,HeightDimensionButton);
				if (GUI.Button(BackRectangle,"Back"))
				{
					GUIPerformance = false;
				}
				if(numeropage<nombreTotalPage)
				{
					if (GUI.Button(new Rect(BackRectangle.x+BackRectangle.width,BackRectangle.y,WidthDimensionButton,HeightDimensionButton),"Next"))
					{
						numeropage++;
					}
				}
				if(numeropage>1)
				{
					if (GUI.Button(new Rect(BackRectangle.x-WidthDimensionButton,BackRectangle.y,WidthDimensionButton,HeightDimensionButton),"Prec"))
					{
						numeropage--;
					}
				}
				AffichagePerformance(numeropage,AffichageResultat,dimX,dimY);

			}
			else
			{
				//Menu principal
				GUI.contentColor = Color.red;
				if (GUI.Button(new Rect(MiddleScreenWidth-(WidthDimensionButton/2),MiddleScreenHeight-HeightDimensionButton-Bord,WidthDimensionButton,HeightDimensionButton),"Play"))
				{
					Stat.GetComponent<StatsPlayer>().setPseudo(stringToEdit);
					Stat.GetComponent<StatsPlayer>().CreationPseudo();
					Stat.GetComponent<StatsPlayer>().setInstruction(true);
					Stat.GetComponent<StatsPlayer>().setLifeUsed(0);
					PlayerPrefs.SetInt("CurrentLvl",0);
					Application.LoadLevel("Lvl1");
				}
				if(GUIContinuer)
				{
					if (GUI.Button(new Rect(MiddleScreenWidth-(WidthDimensionButton/2),MiddleScreenHeight-(HeightDimensionButton/2),WidthDimensionButton,HeightDimensionButton),"Continue"))
					{
						Stat.GetComponent<StatsPlayer>().setPseudo(stringToEdit);
						Stat.GetComponent<StatsPlayer>().CreationPseudo();
						Stat.GetComponent<StatsPlayer>().reloadLife();
						Application.LoadLevel("Lvl" + Stat.GetComponent<StatsPlayer>().getReloadLvl().ToString());
					}
				}
				if(GUI.Button(new Rect(Bord,MiddleScreenHeight-ecart,WidthDimensionButton,HeightDimensionButton),"Parameter")){
					GUIParametre = true;
				}
				if(GUI.Button(new Rect(Screen.width-WidthDimensionButton-Bord,MiddleScreenHeight-ecart,WidthDimensionButton,HeightDimensionButton),"Performance")){
					GUIPerformance = true;
				}		
				if (GUI.Button(new Rect(MiddleScreenWidth-(WidthDimensionButton/2),MiddleScreenHeight+Bord,WidthDimensionButton,HeightDimensionButton),"Quit"))
				{	
					Application.Quit();
				}
				GUI.contentColor = Color.white;
				stringToEdit = GUI.TextField (new Rect (MiddleScreenWidth-(WidthDimensionButton/2),Screen.height-HeightDimensionButton,WidthDimensionButton,HeightDimensionButton/2), stringToEdit, 10);
			}
		}
	}

	void CreationStat()
	{
		var go = GameObject.FindGameObjectsWithTag("Stat");
		Stat = GameObject.Find("Stats");
		if(go.Length>0)
		{
			//already created
			if(start)
			{
				//stringToEdit = Stat.GetComponent<StatsPlayer>().getPseudo();
				stringToEdit = Stat.GetComponent<StatsPlayer>().LastPseudoPlayed();
				start = false;
			}

		}
		else{
			var StatObject = new GameObject("Stats");
			StatObject.AddComponent<StatsPlayer>();
			StatObject.gameObject.tag = "Stat";
			Stat = GameObject.Find("Stats");
			Stat.GetComponent<StatsPlayer>().setSkinfacteur(false);
			}
		if(PlayerPrefs.HasKey("Music"))
		{
			Stat.GetComponent<StatsPlayer>().setMusic(PlayerPrefs.GetInt("Music"));
		}
		if(Stat.GetComponent<StatsPlayer>().getReloadLvl() != 0)
		{
			GUIContinuer = true;
		}
		else
		{
			GUIContinuer = false;
		}

	}

	// 4 temps par page
	void AffichagePerformance(int page, Rect AffichageResultat, float dimX, float dimY)
	{
		//Debut des temps a afficher
		int number = 0;
		switch(page)
		{
			case 1: number = 0;
				break;
			case 2: number = 4;
				break;
		}
		var dimensionLabel = AffichageResultat.height - GUI.skin.box.fontSize;
		GUI.Box(AffichageResultat ,"Best result");
		var firstLigne = new Rect(AffichageResultat.x+10,AffichageResultat.y+GUI.skin.box.fontSize,2*dimX,dimensionLabel/4);
		
		GUI.Label(firstLigne,"Level " + (number+1) + " : " + Stat.GetComponent<StatsPlayer>().Performance(number));
		GUI.Label(new Rect(firstLigne.x,firstLigne.y+dimensionLabel/4,2*dimX,dimensionLabel/4),"Level " + (number+2) + " : " + Stat.GetComponent<StatsPlayer>().Performance(number+1));
		GUI.Label(new Rect(firstLigne.x,firstLigne.y+2*dimensionLabel/4,2*dimX,dimensionLabel/4),"Level " + (number+3) + " : " + Stat.GetComponent<StatsPlayer>().Performance(number+2));
		GUI.Label(new Rect(firstLigne.x,firstLigne.y+3*dimensionLabel/4,2*dimX,dimensionLabel/4),"Level " + (number+4) + " : " + Stat.GetComponent<StatsPlayer>().Performance(number+3));

	}

}
